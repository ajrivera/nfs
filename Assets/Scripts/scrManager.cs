﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class scrManager : MonoBehaviour
{
    public Image hpbar;
    public GameObject hpwhole;
    public Loadout loadout;
    public GameObject car;

    public TextMeshProUGUI pbtime;
    public TextMeshProUGUI timer;

    public TextMeshProUGUI win;
    public TextMeshProUGUI lose;
    public TextMeshProUGUI laps;
    public TextMeshProUGUI waypoints;
    public TextMeshProUGUI hp;


    public AudioSource ost;
    private float StartTime;
    private float TimerControl;
    // Start is called before the first frame update
    void Start()
    {
        StartTime = Time.time;
        minhp();
        win.enabled = false;
        lose.enabled = false;
        
        ost.loop = true;
        ost.Play();

        string mins = ((int)loadout.pbtime / 60).ToString("00");
        string segs = (loadout.pbtime % 60).ToString("00");
        string milisegs = ((loadout.pbtime * 100) % 100).ToString("00");

        string TimerString = string.Format("{00}:{01}:{02}", mins, segs, milisegs);
        pbtime.text = "PB: "+TimerString.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        if (!win.enabled && !lose.enabled)
        {
            TimerControl = Time.time - StartTime;
            string mins = ((int)TimerControl / 60).ToString("00");
            string segs = (TimerControl % 60).ToString("00");
            string milisegs = ((TimerControl * 100) % 100).ToString("00");

            string TimerString = string.Format("{00}:{01}:{02}", mins, segs, milisegs);
            //print(TimerControl);
            timer.text = "Time: "+TimerString.ToString();
        }
        if (loadout.hp <= 0 && !lose.enabled)
        {
            laps.enabled = false;
            waypoints.enabled = false;
            hpwhole.SetActive(false);
            timer.enabled = false;
            pbtime.enabled = false;
            lose.enabled = true;
            ost.Stop();
        }
    }
    public void minhp()
    {
        float hpconv = (loadout.hp * 200) / 500;
        hpbar.GetComponent<RectTransform>().sizeDelta = new Vector2(hpconv, hpbar.GetComponent<RectTransform>().sizeDelta.y);
        hp.text = loadout.hp + "/" + loadout.maxhp;
    }

    public void updateHUD(int lap,int nlaps, int waypoints, int wplenght)
    {
        laps.text = lap + "/" + nlaps;
        this.waypoints.text = waypoints + "/" + wplenght;
    }

    public void won()
    {
        if(loadout.pbtime>TimerControl || loadout.pbtime == 0f)
        {
            loadout.pbtime = TimerControl;
            print("new pb, good job");
        }
        laps.enabled = false;
        waypoints.enabled = false;
        hpwhole.SetActive(false);
        win.enabled = true;

    }

}
