﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AxleInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor;
    public bool steering;
}
public class scrVehicleController : MonoBehaviour
{
    public Loadout loadout;
    public scrManager manager;

    public List<AxleInfo> axleInfos;
    public float maxMotorTorque;
    public float maxSteeringAngle;
    private float brakevalue = -800;
    public float brakes;
    public WheelCollider[] wheels;
    //private bool rearsidebool = true;
    public GameObject[] rearsidelights;
    public Transform CoM;
    public Animator carAni;
    private float vAxis;
    private float hAxis;
    Rigidbody rb;

    public scr_waypoints waypoints;

    public float velocityLimit;

    private float lastVel;
    // finds the corresponding visual wheel
    // correctly applies the transform
    public void ApplyLocalPositionToVisuals(WheelCollider collider)
    {
        if (collider.transform.childCount == 0)
        {
            return;
        }

        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = CoM.localPosition;


        maxMotorTorque = loadout.motor;
        maxSteeringAngle = loadout.steeringAngle;
        brakevalue = loadout.brake;
        rb.angularDrag = loadout.angulardrag;
        rb.mass = loadout.mass;
        velocityLimit = loadout.maxVel;
        loadout.hp = loadout.maxhp;

    }

    public void FixedUpdate()
    {
        if ((Mathf.Abs(rb.velocity.x) + Mathf.Abs(rb.velocity.y) + Mathf.Abs(rb.velocity.z)) > velocityLimit && vAxis > 0)
        {
            //print("HOLA");
            Vector3 NormalizedVector = Vector3.Normalize(new Vector3(rb.velocity.x, rb.velocity.y, rb.velocity.z));
            rb.velocity = NormalizedVector * velocityLimit;
        }
        float motor = maxMotorTorque * vAxis;
        float steering = maxSteeringAngle * hAxis;

        foreach (AxleInfo axleInfo in axleInfos)
        {
            if (axleInfo.steering)
            {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (axleInfo.motor)
            {
                axleInfo.leftWheel.motorTorque = motor;
                axleInfo.rightWheel.motorTorque = motor;
            }
            ApplyLocalPositionToVisuals(axleInfo.leftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel);
        }
        if (vAxis > 0 && (Mathf.Abs(rb.velocity.x) + Mathf.Abs(rb.velocity.y) + Mathf.Abs(rb.velocity.z)) < 4)
        {
            //print("hola");
            rb.AddRelativeForce(Vector3.forward * 300);
        }
    }

    private void Update()
    {
        if (Time.frameCount % 5 == 0)
            lastVel = (Mathf.Abs(rb.velocity.x) + Mathf.Abs(rb.velocity.y) + Mathf.Abs(rb.velocity.z));

        vAxis = Input.GetAxis("Vertical");
        hAxis = Input.GetAxis("Horizontal");
        //print(vAxis);
        //print(hAxis);

        if (Input.GetKey(KeyCode.Space))
        {
            brakes = brakevalue;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            brakes = 0;
        }
        foreach (WheelCollider wc in wheels)
        {
            wc.brakeTorque = brakes;
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            if (carAni.GetInteger("Lights") == 1)
                carAni.SetInteger("Lights", 0);
            else
                carAni.SetInteger("Lights", 1);

        }
        if (vAxis < 0)
        {
            //if (rearsidebool)
            //{
            //Vector3 NormalizedVector = Vector3.Normalize(new Vector3(rb.velocity.x, rb.velocity.y, rb.velocity.z));
            //rb.velocity = NormalizedVector * -1;
            rearsidelights[0].GetComponent<Light>().enabled = true;
            rearsidelights[1].GetComponent<Light>().enabled = true;
            //    rearsidebool = false;
            //}
        }
        else
        {
            rearsidelights[0].GetComponent<Light>().enabled = false;
            rearsidelights[1].GetComponent<Light>().enabled = false;
        }

        if (((Clamp360(transform.rotation.eulerAngles.z) > 45 && Clamp360(transform.rotation.eulerAngles.z) <= 180) || (Clamp360(transform.rotation.eulerAngles.z) > 180 && Clamp360(transform.rotation.eulerAngles.z) < 315)))
        {
            Vector3 rot = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
            Quaternion endRotation = Quaternion.Euler(rot);
            transform.rotation = Quaternion.Lerp(transform.rotation, endRotation, (1 * hAxis) * Time.deltaTime);
        }

        
        //print(Mathf.Abs(rb.velocity.x) + Mathf.Abs(rb.velocity.y) + Mathf.Abs(rb.velocity.z));
    }

    private void OnCollisionEnter(Collision collision)
    {
        StartCoroutine("coldetected");
    }

    IEnumerator coldetected()
    {
        float res = Mathf.Abs(lastVel - (Mathf.Abs(rb.velocity.x) + Mathf.Abs(rb.velocity.y) + Mathf.Abs(rb.velocity.z)));
        if (res > 4)
        {
            print("me hiso daño : "+ (int)res * 10);
            loadout.hp -= (int)res * 10;
            manager.minhp();
        }
        print(loadout.hp);
        yield return new WaitForSeconds(0.2f);

        
        
    }

    

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "wp")
        {
            print("waypoint passed");
            waypoints.nextwaypoint();
        }
    }

    public static float Clamp360(float eulerAngles)
    {
        float result = eulerAngles - Mathf.CeilToInt(eulerAngles / 360f) * 360f;
        if(result < 0)
        {
            result += 360f;
        }
        return result;
    }
}

