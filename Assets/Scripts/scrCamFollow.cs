﻿using UnityEngine;
using System.Collections;

public class scrCamFollow : MonoBehaviour {

    // Variables
    public Transform Target;        // Objetivo a seguir.

    public float camSPD = 3f;       // Velocidad de la cámara.
    public Vector3 offset = new Vector3(0f, 1.5f, -10f);     // Offset de la cámara.
    private bool smooth = true;     // Booleana de smooth Activo o Inactivo.


    void Start ()
    {

    }
	
    void Update()
    {
        if (Target.gameObject.activeSelf)
        {
            Vector3 endPosition = new Vector3(Target.localPosition.x + offset.x, Target.localPosition.y + offset.y, Target.localPosition.z + offset.z);       // Decidimos la posicion de seguimiento de la cámara.
            Vector3 rot = new Vector3(0, Target.rotation.eulerAngles.y, 0);
            Quaternion endRotation = Quaternion.Euler(transform.rotation.x, rot.y, transform.rotation.z);
            if (smooth) transform.position = Vector3.Lerp(transform.position, endPosition, camSPD * Time.deltaTime);        // Amplicamos el smooth.
            else transform.position = endPosition;
            if (smooth) transform.rotation = Quaternion.Lerp(transform.rotation, endRotation, camSPD * Time.deltaTime);
            else transform.rotation = endRotation;
        }
    }

	void FixedUpdate ()
    {

    }
}
