﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class Loadout : ScriptableObject
{
    public float mass;
    public float motor;
    public float brake;
    public float angulardrag;
    public float steeringAngle;
    public int maxhp;
    public int hp;
    public float maxVel;
    public float pbtime;


}
