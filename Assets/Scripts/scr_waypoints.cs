﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_waypoints : MonoBehaviour
{
    public GameObject[] waypoints;
    public scrManager manager;
    private int dot;
    public int laps;
    private int lapsdone;
    private bool win;
    
    // Start is called before the first frame update
    void Start()
    {

        lapsdone = 1;
        manager.updateHUD(lapsdone, laps, dot, waypoints.Length);
        foreach (GameObject wp in waypoints)
        {
            wp.SetActive(false);
        }
        waypoints[0].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        


    }

    public void nextwaypoint()
    {
        if (!win)
        {
            dot++;
            print(dot);
            if (dot >= waypoints.Length)
            {
                if ((lapsdone - 1) < laps)
                {
                    waypoints[dot - 1].SetActive(false);
                    dot = 0;
                    waypoints[dot].SetActive(true);
                    lapsdone++;
                    if (lapsdone - 1 == laps)
                        dot = waypoints.Length;
                    print("You have done laps: " + lapsdone + "/" + laps);
                }
                else
                {
                    if (waypoints[0].activeSelf)
                    {
                        win = true;
                        print("you have won");
                        manager.won();
                        waypoints[0].SetActive(false);
                    }
                    else
                    {
                        print(dot);
                        waypoints[dot - 1].SetActive(false);
                        waypoints[0].SetActive(true);
                    }


                }
            }
            else
            {
                waypoints[dot - 1].SetActive(false);
                waypoints[dot].SetActive(true);
            }
            if (lapsdone - 1 == laps)
                manager.updateHUD(lapsdone-1, laps, dot, waypoints.Length);
            else
                manager.updateHUD(lapsdone, laps, dot, waypoints.Length);
        }
        else
        {
            manager.won();
        }
        
    }
}
