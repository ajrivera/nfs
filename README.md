# Inicio
Juego racing, consiste en seguir las luces hi hacer el mejor tiempo. No he llegado a implementar el poder reiniciar desde la misma escena.

# Controles

| Teclas | Acción |
| ------ | ------ |
| W/S | `Acelerar/Desacelerar` |
| A/D | `Steering` |
| spacebar | `brake` |
| F | `lights` |

# Requisitos Mínimos

- 3D
- Ha d’haver càmara 3D, que no pot ser fixa, i hauria de ser en tercera persona
- Ha d’haver com a mínim una font d’il·luminació complexa, i varies en general
- Ha d’haver moviment en les tres dimensions
- Ha d’haver físiques 3D
- Ha d’haver un sistema rotacional
- El joc ha de seguir un dels següents camins
- Pot ser un Racer estil Super Mario Kart, amb salts i moviment en eix Y. En aquest cas no necessita físiques molt complexes, pero s’ha de compensar mitjançant objectes, turbos, etc.
- Pot ser un joc amb físiques realistes estil F1. En aquest cas no es demanara moviment en eix Y pero sí físiques realistes. Això no significa que pugui ser un mon pla. Ha d’haver pujades i baixades encara que siguin de 10º
- Pot ser un plataformes 3D. En aquest cas el moviment ha de ser lliure en 3D. No feu armes de projectils. NO es un FPS.
- S’ha de fer servir scriptable objects

# Ampliaciones
quizas me dejo algo

- Us de Managers
- Materials 3D
- Us del Package Manager y la Asset Store
- Suport per controller (Se ha intentado pero no he tenido tiempo y me destruyo el proyecto)
- No se si es una ampliacion pero he implementado un sistema de daño al coche dependiendo de 5 frames antes del choque la diferencia de velocidad, 
tambien he hecho una animacion para que los faros salgan y se enciendan ja que el modelo del coche es de estos que tienen ocultos los faros.
- Si el coche esta bolcado pulsando a/d o ←/→ se gira para volver a estar correcto.
